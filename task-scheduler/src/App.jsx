function App() {
  return (
    <div>
      <h1 className="text-3xl font-bold text-center underline">Hello world!</h1>
      <div className="radial-progress" style={{ '--value': 70 }}>
        70%
      </div>
    </div>
  );
}

export default App;
// https://task-scheduler-4b993-default-rtdb.europe-west1.firebasedatabase.app
